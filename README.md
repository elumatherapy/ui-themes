# ui-themes

# INSTALLATION
To install the eluma ui-theme, run the following
```
npm install git@bitbucket.org:elumatherapy/ui-themes.git --save
```

This will install ui-themes package, which contains one exported `theme.scss` file.
Import this file from the package into your entry/main scss file.

```
@import 'eluma-ui-themes/bundled';
```

# CONTRIBUTING

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Bundles and packages scss/min.css theme files into npm tarball
```
npm run bundle-package
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Run your end-to-end tests
```
npm run test:e2e
```

### Run your unit tests
```
npm run test:unit
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
